import unittest
from multnotk import multNotK
from fastmultnotk import fastMultNotK

class unitTest(unittest.TestCase):

    def testTrue(self):
        self.assertEqual(multNotK([1,2,3,4,5]), [120, 60, 40, 30, 24])
    def testTrue(self):
        self.assertEqual(multNotK([3, 2, 1]), [2, 3, 6])
    def testTrue(self):
        self.assertEqual(fastMultNotK([1,2,3,4,5]), [120, 60, 40, 30, 24])
    def testTrue(self):
        self.assertEqual(fastMultNotK([3, 2, 1]), [2, 3, 6])

if __name__ == '__main__':
    unittest.main()