# My implementation of the solution

# Iterate forward/backward through the array 
# accumulating successive multiplications
#
def forward(a):
    acc = 1
    result = []
    for n in a:
        acc = acc * n
        result.append(acc)
    return result

def backward(a):
    acc = 1
    result = []
    for n in reversed(a):
        acc = acc * n
        result.append(acc)
    return list(reversed(result))

def fastMultNotK(a):
    fw = forward(a)
    bw = backward(a)
    l = len(a)
    result = []
    for i in range(l):
        # First and last case 
        if i == 0:
            result.append(bw[i + 1])
        elif i == len(a) - 1:
            result.append(fw[i - 1])
        # Inside first and last case
        else:
            result.append(bw[i + 1] * fw[i - 1])
    return result
