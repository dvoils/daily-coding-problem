
# Solution 1
#
def first_missing_positive1(nums):
    if not nums:
        return 1
    for i, num in enumerate(nums):
        while i + 1 != nums[i] and 0 < nums[i] <= len(nums):
        #    print nums
            v = nums[i]
            nums[i], nums[v - 1] = nums[v - 1], nums[i]
            if nums[i] == nums[v - 1]:
                break
        #print nums, "found", i + 1
    
    for i, num in enumerate(nums, 1):
        if num != i:
            return i
    return len(nums) + 1

# Solution 2
#
def first_missing_positive2(nums):
    s = set(nums)
    i = 1
    while i in s:
        i += 1
    return i