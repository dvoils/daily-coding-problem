import random

#
def use_an_array(A):
    a = 1

def swap(i,j,A):
    temp = A[i]
    A[i] = A[j]
    A[j] = temp
    return A

def search(i,l,A):
    done = False
    while not done:
        if A[i] > l:
            done = True
        if A[i] < 1:
            done = True
        else:
            j = A[i] - 1
            A = swap(i,j,A)
            if A[i] == A[j]:
                done = True
    return A
    
def get_result(A):
    done = False
    i = 0
    
    while not done:
        if i+1 != A[i]:
            result = i+1
            done = True
        i = i+1
    return result

# my rewrite of the solution
#
def mysolution(A):
    l = len(A)
    for i in range(0,l):
        A = search(i,l,A)
    result = get_result(A)
    return result

# generate a random permutation
#
def permutation(N):
    A = range(1,N+1)
    R = []
    for i in range(1,N):
        r = random.randint(1,len(A)-1)
        x = A.pop(r)
        R.append(x)
    
    r = random.randint(1,len(R)-1)
    x = A[0]
    R.insert(r,x)
    return R
