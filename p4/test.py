import unittest
from p4 import permutation, mysolution
from solution import first_missing_positive1, first_missing_positive2

class unitTest(unittest.TestCase):

    
    def test_mysolution(self):
        result = mysolution([9, 2, -4, -1, 6, -6, 3, 5, 1, 4, 7])
        self.assertEqual(result, 8)

    def test_solution1(self):
        R = permutation(10)
        x = R.pop()

        result = first_missing_positive1(R)
        self.assertEqual(result, x)


    def test_solution2(self):
        result = first_missing_positive2([9, 2, -4, -1, 6, -6, 3, 5, 1, 4, 7])
        self.assertEqual(result, 8)
    
if __name__ == '__main__':
    unittest.main()
