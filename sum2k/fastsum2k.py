# 012345678
# 123456789
# l       r
# m = (0 + 8)/2 = 4

# 012345678
# 123456789
# l   m   r


def binarySearch(A,n):
    l = 0
    r = len(A) - 1
    while l < r:
        m = (l + r)/2
        if n > A[m]:
            l = m + 1
        else:
            r = m
    if n == A[l]:
        return l
    return False

def fastSum2K(a, k):
    # Sort the array to perform binary search which is O(log(n)) time
    a.sort()

    # For every element in the list, determine if the
    # number t, required to complete the sum, is in the list
    #
    l = len(a)
    for i in range(l):
        t = k - a[i]
        f = binarySearch(a,t)
    
        if f != False:
            if f != i:
                return True
    return False
