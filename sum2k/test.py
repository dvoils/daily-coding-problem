import unittest
from sum2k import sum2k
from solution import twoSum

class unitTest(unittest.TestCase):

    def testTrue(self):
        self.assertEqual(sum2k([1,2,3,4,5], 5), True)
    def testTrue(self):
        self.assertEqual(sum2k([1,2,3,4,5], 2), False)
    def testTrue(self):
        self.assertEqual(twoSum([1,2,3,4,5], 5), True)
    def testTrue(self):
        self.assertEqual(twoSum([1,2,3,4,5], 2), False)

if __name__ == '__main__':
    unittest.main()