# Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
# For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
# This solution is O(n^2)
#
def sum2k(a,n):
    l = len(a)
    for i in range(0,l):
        for j in range(i + 1,l):
            if a[i] + a[j] == n:
                return True
    return False
