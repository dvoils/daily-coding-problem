import time
from sum2k import sum2k
from solution import twoSum
from fastsum2k import fastSum2K

A = range(10000)

start_time = time.time()
print sum2k(A,0)
print "sum2k", time.time() - start_time

start_time = time.time()
print fastSum2K(A,0)
print "fastSum2K", time.time() - start_time

start_time = time.time()
print twoSum(A,0)
print "solution", time.time() - start_time
