class Node:
    def __init__(self, val, left=None, right=None):
            self.val = val
            self.left = left
            self.right = right

def serialize(root):
    if root is None:
        return '#'
    return '{} {} {}'.format(root.val, serialize(root.left), serialize(root.right))

def deserialize_original(data):
    def helper():
        val = next(vals)
        if val == '#':
            return None
        node = Node(val)
        node.left = helper()
        node.right = helper()
        return node
    vals = iter(data.split())
    return helper()

def deserialize(data):
    def helper():
        if len(data) > 0:
            val = data.pop(0)
        else: return
        print "node", val
        node = Node(val)
        print "left", val
        node.left = helper()
        print "right", val
        node.right = helper()
        return node
    return helper()


#node = Node('root', Node('left', Node('left.left')), Node('right'))
#print deserialize(serialize(node)).val