import unittest
from bst import BinarySearchTree
from bstSerial import bst_serialize
from bstSerial import deserialize
from bstSerial import serialize
from solution import Node

class unitTest(unittest.TestCase):

    def testSerialize1(self):
        A = [15,5,16,3,12,20,10,13,18,23,6,7]
        bst = BinarySearchTree(A)
        
        S = bst_serialize(bst.T)
        A.sort()
        self.assertEqual(S,A)

    def testSerialize2(self):
        node = Node('root', Node('left', Node('left.left')), Node('right'))
        temp1 = serialize(node)
        print temp1
        temp2 = deserialize(temp1, Node)
        print temp2
        

if __name__ == '__main__':
    unittest.main()
