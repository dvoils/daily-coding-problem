# Serialize tree from "Introduction to Algorithms"
#
def bst_serialize(root):
    current = root
    stack = []
    result = []
    done = 0
      
    while True:
      
        if current is not None:
            stack.append(current)
            current = current.left
            
        elif(stack):
            current = stack.pop()
            result.append(current.key)
            current = current.right
        
        else:
            return result

# serialize tree used for the challenge test
#
def serialize(root):
    current = root
    stack = []
    result = []
    done = 0
      
    while True:
      
        if current is not None:
            stack.append(current)
            current = current.left
            
        elif(stack):
            current = stack.pop()
            result.append(current.val)
            current = current.right
        
        else:
            return result

#                 o
#            /         \
#         l                r
#      /    \           /     \
#    ll      lr       rl       rr
#   / \     /  \     /  \     /  \
# lll llr  lrl lrr  rll rlr  rrl rrr


 # Deserialize challenge tree
 # Todo: find the solution
 #
def deserialize(data, node):
    return True

