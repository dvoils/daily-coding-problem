# generate a random permutation
#
def permutation(N):
    A = range(1,N+1)
    R = []
    for i in range(1,N):
        r = random.randint(1,len(A)-1)
        x = A.pop(r)
        R.append(x)
    
    r = random.randint(1,len(R)-1)
    x = A[0]
    R.insert(r,x)
    return R
