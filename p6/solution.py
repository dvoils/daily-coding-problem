# O(2^N)
def num_encodings1(s):
    print "****************** ", s, s[:2]

    if s.startswith('0'):
        print "starts with 0"
        return 0
    elif len(s) <= 1: # This covers empty string
        print "empty string"
        return 1

    total = 0
    
    if int(s[:2]) <= 26:
        print "search 2"
        total += num_encodings1(s[2:])
    print "search 1"
    total += num_encodings1(s[1:])
    return total

# O(N)
from collections import defaultdict

def num_encodings2(s):
    # On lookup, this hashmap returns a default value of 0 if the key doesn't exist
    # cache[i] gives us # of ways to encode the substring s[i:]
    cache = defaultdict(int)
    cache[len(s)] = 1 # Empty string is 1 valid encoding
    print cache
    for i in reversed(range(len(s))):
        print "position", i, i+2
        if s[i].startswith('0'):
            cache[i] = 0
        elif i == len(s) - 1:
            print "case1"
            cache[i] = 1
        else:
            print "case2", s[i:i + 2], s[i+2:]
            if int(s[i:i + 2]) <= 26:
                print "case3", cache[i], cache[i+2]
                cache[i] = cache[i + 2]
            cache[i] += cache[i + 1]
    
    for i in range(len(s)):
        print "cache", cache[9]
    
    return cache[0]