import unittest
from p6 import encode, rand_char, mysolution
from solution import num_encodings1, num_encodings2

class unitTest(unittest.TestCase):

    @unittest.skip("reason for skipping")
    def test_solution1(self):
        s = rand_char(5)
        s = 'guhpf'
        print s
        q = encode(s)
        r = num_encodings1(q)
        print r
        self.assertEqual(1,1)

    @unittest.skip("reason for skipping")
    def test_solution2(self):
        s = 'guhpf'
        print s
        q = encode(s)
        print q
        r = num_encodings2(q)
        print r
        self.assertEqual(1,1)
    
    def test_mysolution(self):
        s = 'guhpf'
        print s
        q = encode(s)
        print q
        mysolution(q)



if __name__ == '__main__':
    unittest.main()
